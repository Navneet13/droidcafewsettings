

/*
     Navneet Kaur : C0740854
 */

package com.example.droidcafewsettings;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.widget.Button;

import static org.hamcrest.CoreMatchers.equalTo;
import static junit.framework.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.robolectric.Shadows.shadowOf;

import org.junit.Before;
import org.junit.Test;

import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowActivity;
import org.robolectric.shadows.ShadowIntent;
import org.robolectric.shadows.ShadowToast;

import android.widget.EditText;
import android.widget.ImageView;

@RunWith(RobolectricTestRunner.class)

public class MainActivityTest {

    private MainActivity activity;
    private OrderActivity orderActivity;

    @Before
    public void setUp() throws Exception {
        activity = Robolectric.buildActivity(MainActivity.class)
                .create()
                .resume()
                .get();

    }

    @Test
    public void activityIsNotNull() throws Exception {
        assertNotNull(activity);
    }


    // TC1: Ordering a Frozen Yogurt - When user orders a frozen yogurt, a Toast appears with the following message: “You ordered a FroYo”

    @Test
    public void testOrderedFrozenYogurt() {
        // 1. Find the ID of the ImageView
        ImageView imageView = (ImageView) activity.findViewById(R.id.froyo);

        // 2. Click on the button
        imageView.performClick();

        //3. Get the Toast  -> Get the message of the toast
        String toastMessage = ShadowToast.getTextOfLatestToast().toString();

        //4. Check if the expected Output is equal to the text in the toast
        assertThat(toastMessage, equalTo("You ordered a FroYo."));

    }

    // TC2:  Cart page remembers user info - Test that the app remembers the information the user entered on the Shopping Cart page.

    @Test
    public void testCartPageRememberUserInfo() throws InterruptedException {

        //1a. Get the FloatingActionButton ID
        FloatingActionButton floatingActionButton = (FloatingActionButton) activity.findViewById(R.id.fab);

        //1b. Click the button
        floatingActionButton.performClick();

        //2a. Check if the next activity is reached
        ShadowActivity shadowActivity = shadowOf(activity);
        Intent startedIntent = shadowActivity.getNextStartedActivity();
        ShadowIntent shadowIntent = shadowOf(startedIntent);
        assertThat(shadowIntent.getIntentClass().getName(), equalTo(OrderActivity.class.getName()));


        //3a. To access the elements of the OrderActivity
        orderActivity = Robolectric.buildActivity(OrderActivity.class)
                .create()
                .resume()
                .get();

        //3b. Send the inputs or Fill the fields
        EditText name = (EditText) orderActivity.findViewById(R.id.name_text);
        name.setText("Navneet");
        EditText address = (EditText) orderActivity.findViewById(R.id.address_text);
        address.setText("Toronto");
        EditText phone = (EditText) orderActivity.findViewById(R.id.phone_text);
        phone.setText("9876272564");

        //3c.  Get the 'Save Customer Info' button's ID and click that button
         orderActivity.findViewById(R.id.saveButton).performClick();

        //4. Get back to the elements of MainActivity
        activity = Robolectric.buildActivity(MainActivity.class)
                .create()
                .resume()
                .get();

        //5. Opens the OrderActivity again   -> By clicking the Floating Action Button again
        floatingActionButton.performClick();

        //7. Check that if the Sent Inputs are equal to the text from TextViews OR id the information is saved or not
        assertThat(name.getText().toString(), equalTo("Navneet"));
        assertThat(address.getText().toString(), equalTo("Toronto"));
        assertThat(phone.getText().toString(), equalTo("9876272564"));
    }


}
